﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericConstr
{
    class Program
    {
        class Shape
        {
            public string ShapeToolTip { get; set; }
            public Shape()
            {
            
            }
        }
        class Circle : Shape
        {
            public float Radius { get; set; }
            public Circle() 
            {
               
            }
        }
        class Triangle : Shape
        {
            public float A { get; set; }
            public float B { get; set; }
            public float C { get; set; }
            public Triangle() 
            {

            }
        }
        class Bag { }

        class MyToolTip<T> where T :  Shape, new()
        {
            private T m_shape;
            public MyToolTip(T shape)
            {
                m_shape = shape;
                if (m_shape == null)
                    m_shape = new T();

            }
            public string GetToolTip()
            {
                return $"data ..... {m_shape.ShapeToolTip}";
            }
        }

        class MyList<T> where T : IComparable<T>
        {
            List<T> m_list;
            public void Sort()
            {
                m_list.Sort();
            }
            public MyList(List<T> list)
            {
                m_list = list;
            }
        }

        class MyMath
        {
            public T GetMax<T>(T t1, T t2)
            {
                return t1;
            }
        
            static public T Creator<T>() where T : new()
            {
                return new T();
            }

            public float Get4DigitAfterDot(float f)
            {
                return 0;
            }
            public int MaxInt(int x1, int x2)
            {

                return x1 > x2 ? x1 : x2;

                /*
                int res = x1 > x2 ? x1 : x2;
                if (x1 > x2)
                    res = x1;
                else
                    res = x2;
                return res;

                //Circle c = null;
                //Circle d = c ?? new Circle();
                //if (c != null)
                //    d = c;
                //else
                //    d = new Circle();
                */
            }
        }

        // Tuple<int, int> tup1 = new Tuple<int, int>(3, 4);
        // tup1.Item1 = 1; // error
        class MyTuple<T1,T2>
        {
            public T1 Item1
            {
                get;
                private set;
            }
            public T2 Item2
            {
                get;
                private set;
            }

            public MyTuple(T1 t1, T2 t2)
            {
                Item1 = t1;
                Item2 = t2;
            }
        }

        static void Main(string[] args)
        {
            Circle c = new Circle
            {
                ShapeToolTip = "nice 3d circle",
                Radius = 7.1f
            };
            Shape s = c;

            MyToolTip<Shape> myToolTipShape = new MyToolTip<Shape>(c);

            MyTuple<int, string> myTupleIntString = new MyTuple<int, string>(3, "three");
            Console.WriteLine(myTupleIntString.Item1);
            Console.WriteLine(myTupleIntString.Item2);
            Tuple<int, string> tupleIntString = new Tuple<int, string>(3, "three");
            Console.WriteLine(tupleIntString.Item1);
            Console.WriteLine(tupleIntString.Item2);

            Circle new_circle = MyMath.Creator<Circle>();
            Bag bag1 = MyMath.Creator<Bag>();
            //int i2 = 5; // new Int32();
            
            //int i = MyMath.Creator<Int32>(); // no ctor() for Int32

            //MyToolTip<Int32> toolTip = new MyToolTip<int>();

            //MyToolTip<Circle> tool = new MyToolTip<Circle>(c);
            // MyToolTip<Bag> tool1 = new MyToolTip<Bag>(new Bag()); // ERROR! Bag not inherit from Shape

        }
    }
}
